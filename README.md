# OPENFOAM RECIPES

This repo is meant to work as a collection of recipes for doing stuff with OpenFOAM. That thing you've done 2 years ago and now need again, but forgot how to, so you're wasting time on cfd-online.

## Contents

- `average_yplus_on_patch`.
Compute the average $`y^+`$ on a patch at each time-step.
Uses `swak4foam`, serves as agood example on how to compute aggregate of expressions defined on patches. 

- `k_budget`.
Compute the terms in the $`k`$ and $`u_i u_j`$ equations.
Uses `swak4foam`, a good example of complicated `expressionField`s.

- `googletest`.
Compile googletest and google mock using OpenFOAM compiler flags. Adapted from https://sourceflux.de/blog/google-test-and-openfoam

- `blockMeshCylinder`.
Tutorial to build a cylinder mesh with boundary layer using: python, m4, blockMesh, snappyHexMesh. Authored by johannep. 

- `foamSetValue`.
Small recipe to change singleValued inputs of an OpenFOAM case via bash-scripts . Includes example of use in sbatch script on vera. Authored by johannep. 

## Contributing

- Add a folder with a reasonable name, which will contain the recipe.
- Add a recipe.md file, in which you describe the recipe.
You can do *a lot* with markdown, see https://docs.gitlab.com/ee/user/markdown.html.
It is a very good idea to include what the prerquisites for using the recipe are, e.g what OpenFOAM version it works for.

- Add any other necessary files.
- Add a one-liner about your recipe to the contents section above.
