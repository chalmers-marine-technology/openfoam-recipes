#!/bin/sh
#
#SBATCH -n 16
#
#SBATCH -p vera
#
#SBATCH -A C3SE2019-1-1 
#
#SBATCH -t 48:00:00
#
#SBATCH -J tmp
#
#SBATCH -o slurm-%j.out
#

# Load native OFv1806 module: 
module load iccifort/2018.3.222-GCC-7.3.0-2.30  impi/2018.3.222 OpenFOAM/v1806
source $FOAM_BASH 

mkdir logs;

foamSetValue numberOfSubdomains $SLURM_NTASKS system/decomposeParDict
decomposePar &> "logs/dec"
mpirun interFoam -np $SLURM_NTASK  -parallel  &> "logs/interFoam"
reconstructPar &> "logs/rec"

# note: I dont think the -np flag is needed at all. It says not on c3se homepage. 
