## foamSetValue
A really small sed -i alias that I use for changing input via bash-scripts in OpenFOAM. See the foamSetValue file for details. 
See also the example usage in sbatch\_vera.sh where it is used to make decomposeParDict match the submitted number of tasks in SLURM.   

happy coding and good luck
Johannes Palm
2019-05-28
