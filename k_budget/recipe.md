# Compute the terms of the $`k`$ budget in LES/DNS

A set of function objects and dictionaries for use with `funkySetFields` are provided,
allowing to compute the resolved terms in the $`k`$-equation of an LES/DNS.
Additionally, similar functionality for computing the budgets of individual
Re-stress terms are provided.

The files have been kindly provided by Saleh Rezaeiravesh at KTH and stylistically modified by Timofey Mukha.

## Prerequisites

* `swak4foam` installed, all versions from the last 6 years will work.
* Any version of OpenFOAM, at least starting 2.1 from the Foundation.

The above is a big benefit of using `swak4foam`, even if the same thing can be done with OpenFOAM natively: `swak` compiles for all major OpenFOAM versions, and it is very careful about breaking changes.
All the things you do now are likely to work in a five years time.

## The recipe

The terms can be split in two groups: those that can be computet a posteriori using the
`UMean` and `UPrime2Mean` fields, and those who can't.
The former are the production, convection and viscous transport terms.
To compute these, `funkySetFields` can be used. 
For the terms in $`k`$, do the following:

- Copy `funkySetFieldsDict.kBudget` to your `system` directory.
- Open the file and set the correct value of $`\nu`$ in the `reqVariables` list. 
- Run `funkySetFields -latestTime -dictExt kBudget`.
The output will tell you what fields have been created.

To compute the terms for individual components, do the same with `funkySetFieldsDict.componentsBudget`.
Please note that since this has originally been written for channel flow, not all of the components are
computed, so you may need to add them. This should be trivial based on the components that are present,
which are $`u'u'`$, $`v'v'`$, $`w'w'`$, and $`u'v'`$.

The terms that cannot be computed a posteriori are viscous dissipation, pressure-velocity transport, and turbulent transport.
These have to be computed on the fly during the course of the simulation.
Asscociated `swak4foam` function objects are provided in files `kBudget` and `componentsBudget`, for the terms in $`k`$ and $`u_i'u_j'`$, respectively.
This include `fieldAverage` objects, to performe averaging in time.
`#include` the associated files in your `controlDict`.

The definitions of the terms in both tensor and vector notation can be found in an appending in Edmond Shehadi's thesis
http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A1198470



