/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  1.7.1                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// general m4 commands - DO NOT ALTER THIS SECTION

changecom(//)changequote([,]) dnl>
define(calc, [esyscmd(perl -e 'use Math::Trig; print ($1)')]) dnl>
define(VCOUNT, 0)
define(vlabel, [[// ]Vertex $1 = VCOUNT define($1, VCOUNT)define([VCOUNT], incr(VCOUNT))])

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// ------------------------ User-defined parameters ------------------------ //

include(domain.m4)

/*
// ---  half height of cylinder --- //
define(top,1.2848)

//----- Cylinder radius -----//
define(r, 1.05)

// RESOLUTIONS: 
// --- Angular resolution --- //
define(Ntheta,4)

// Radial resolutions: 
define(Nr1,2)
define(Nr,4)
define(Nr2,8)


// Vertical resolutions: 
define(Nz0,6)
define(Nz1,4)
define(Nz2,8)
define(z0Grad,1)
define(z1Grad,2)
*/

//----- Always start at 0 0 0 -----//
define(depth,0)

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// ---------------------------- Derived parameters ------------------------- //
//----- Outer cylinder radius -----//
define(top2,calc(2*top))
define(r2, calc(2*r))
define(r3, calc(10*r))
define(magn,1)
define(air,calc(5*top))
define(z2Grad,calc(air/top))
define(rGrad,calc(r2/r))
define(r2Grad,calc(r3/r2))



define(c30,calc(cos(deg2rad(22.5))))
define(s30,calc(sin(deg2rad(22.5))))
define(c45,calc(cos(deg2rad(45))))

// Tuning parameters of cylinder lid, and its arc-points 
define(r1,calc(0.75*r))
define(r1Ax,calc(r1/1.15))
define(acrLid,calc( 0.5*(2*r1*r1*c45*c45 - r1Ax*r1Ax)/(r1*c45-r1Ax) ))
define(arLid,calc(r1Ax - acrLid)) 
// note: s30 and c30 below are not 30 degrees, its 22.5
define(aLidX,calc(arLid*c30+acrLid))  
define(aLidY,calc(arLid*s30))

// used but same as top.
define(r3Top,calc(top*magn))



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


vertices        // FOR QUARTER SYMMETRY
(
// * * * * * LEVEL 0 * * * * * //
// On y-axis
(0 0 depth)								vlabel(v000)
(0 r1Ax depth)							vlabel(v010)
(0 r depth)								vlabel(v020)
(0 r2 depth)							vlabel(v030)
(0 r3 depth)							vlabel(v040)

// On x-axis
(r1Ax 0 depth)								vlabel(v100)
(r 0 depth)								vlabel(v200)
(r2 0 depth)								vlabel(v300)
(r3 0 depth)								vlabel(v400)

// 1st ring 
(calc(r1*c45) calc(r1*c45) depth)			vlabel(v110)
// 2nd ring
(calc(r*c45) calc(r*c45) depth)			vlabel(v220)

// 3rd ring
(calc(r2*c45) calc(r2*c45) depth)			vlabel(v330)

// 4th ring and outside
(calc(r3*c45) calc(r3*c45) depth)			vlabel(v440)

// * * * * * LEVEL 1 * * * * * //
// On y-axis
(0 0 top)								vlabel(v001)
(0 r1Ax top)							vlabel(v011)
(0 r top)								vlabel(v021)
(0 r2 top)							vlabel(v031)
(0 r3 r3Top)							vlabel(v041)

// On x-axis
(r1Ax 0 top)								vlabel(v101)
(r 0 top)								vlabel(v201)
(r2 0 top)								vlabel(v301)
(r3 0 r3Top)								vlabel(v401)

// 1st ring 
(calc(r1*c45) calc(r1*c45) top)			vlabel(v111)
// 2nd ring
(calc(r*c45) calc(r*c45) top)			vlabel(v221)

// 3rd ring
(calc(r2*c45) calc(r2*c45) top)			vlabel(v331)

// 4th ring and outside
(calc(r3*c45) calc(r3*c45) r3Top)			vlabel(v441)


// * * * * * LEVEL 2 * * * * * //
// On y-axis
(0 0 top2)								vlabel(v002)
(0 r1Ax top2)							vlabel(v012)
(0 r top2)								vlabel(v022)
(0 r2 top2)							vlabel(v032)
(0 r3 calc(magn*top2))							vlabel(v042)

// On x-axis
(r1Ax 0 top2)								vlabel(v102)
(r 0 top2)								vlabel(v202)
(r2 0 top2)								vlabel(v302)
(r3 0 calc(magn*top2))								vlabel(v402)

// 1st ring 
(calc(r1*c45) calc(r1*c45) top2)			vlabel(v112)
// 2nd ring
(calc(r*c45) calc(r*c45) top2)			vlabel(v222)

// 3rd ring
(calc(r2*c45) calc(r2*c45) top2)			vlabel(v332)

// 4th ring and outside
(calc(r3*c45) calc(r3*c45) calc(magn*top2))			vlabel(v442)

// * * * * * LEVEL 3 * * * * * //
// On y-axis
(0 0 air)								vlabel(v003)
(0 calc(magn*r1Ax) air)							vlabel(v013)
(0 calc(magn*r) air)								vlabel(v023)
(0 calc(magn*r2) air)							vlabel(v033)
(0 r3 air)							vlabel(v043)

// On x-axis
(calc(magn*r1Ax) 0 air)								vlabel(v103)
(calc(magn*r) 0 air)								vlabel(v203)
(calc(magn*r2) 0 air)								vlabel(v303)
(r3 0 air)								vlabel(v403)

// 1st ring 
(calc(magn*r1*c45) calc(magn*r1*c45) air)			vlabel(v113)
// 2nd ring
(calc(magn*r*c45) calc(magn*r*c45) air)			vlabel(v223)

// 3rd ring
(calc(magn*r2*c45) calc(magn*r2*c45) air)			vlabel(v333)

// 4th ring and outside
(calc(r3*c45) calc(r3*c45) air)			vlabel(v443)


);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
blocks
(
// * * * * * LEVEL 0 * * * * * //

// 1st ring: (r - r2)
hex (v200 v300 v330 v220 v201 v301 v331 v221)
(Nr Ntheta Nz0)
simpleGrading (rGrad 1 z0Grad)

hex (v020 v220 v330 v030 v021 v221 v331 v031)
(Ntheta Nr Nz0)
simpleGrading (1 rGrad z0Grad)

// 2nd ring: (r2 - r3)
hex (v300 v400 v440 v330 v301 v401 v441 v331)
(Nr2 Ntheta Nz0)
simpleGrading (r2Grad 1 z0Grad)

hex (v030 v330 v440 v040 v031 v331 v441 v041)
(Ntheta Nr2 Nz0)
simpleGrading (1 r2Grad z0Grad)

// * * * * * LEVEL 1 * * * * * //
// Lid : 
hex (v001 v101 v111 v011 v002 v102 v112 v012)
(Ntheta Ntheta Nz1)
simpleGrading (1 1 z1Grad)

hex (v101 v201 v221 v111 v102 v202 v222 v112)
(Nr1 Ntheta Nz1)
simpleGrading (1 1 z1Grad)

hex (v011 v111 v221 v021 v012 v112 v222 v022)
(Ntheta Nr1 Nz1)
simpleGrading (1 1 z1Grad)

// 1st ring: (r - r2)
hex (v201 v301 v331 v221 v202 v302 v332 v222)
(Nr Ntheta Nz1)
simpleGrading (rGrad 1 z1Grad)

hex (v021 v221 v331 v031 v022 v222 v332 v032)
(Ntheta Nr Nz1)
simpleGrading (1 rGrad z1Grad)

// 2nd ring: (r2 - r3)
hex (v301 v401 v441 v331 v302 v402 v442 v332)
(Nr2 Ntheta Nz1)
simpleGrading (r2Grad 1 z1Grad)

hex (v031 v331 v441 v041 v032 v332 v442 v042)
(Ntheta Nr2 Nz1)
simpleGrading (1 r2Grad z1Grad)

// * * * * * LEVEL 2 * * * * * //

// Lid : 
hex (v002 v102 v112 v012 v003 v103 v113 v013)
(Ntheta Ntheta Nz2)
simpleGrading (1 1 z2Grad)

hex (v102 v202 v222 v112 v103 v203 v223 v113)
(Nr1 Ntheta Nz2)
simpleGrading (1 1 z2Grad)

hex (v012 v112 v222 v022 v013 v113 v223 v023)
(Ntheta Nr1 Nz2)
simpleGrading (1 1 z2Grad)


// 1st ring: (r - r2)
hex (v202 v302 v332 v222 v203 v303 v333 v223)
(Nr Ntheta Nz2)
simpleGrading (rGrad 1 z2Grad)

hex (v022 v222 v332 v032 v023 v223 v333 v033)
(Ntheta Nr Nz2)
simpleGrading (1 rGrad z2Grad)


// 2nd ring: (r2 - r3)
hex (v302 v402 v442 v332 v303 v403 v443 v333)
(Nr2 Ntheta Nz2)
simpleGrading (r2Grad 1 z2Grad)

hex (v032 v332 v442 v042 v033 v333 v443 v043)
(Ntheta Nr2 Nz2)
simpleGrading (1 r2Grad z2Grad)
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

edges           
(
// Lid arcs
arc v101 v111 (aLidX aLidY top)
arc v111 v011 (aLidY aLidX top)

arc v102 v112 (aLidX aLidY top2)
arc v112 v012 (aLidY aLidX top2)

arc v103 v113 (calc(magn*aLidX) calc(magn*aLidY) air)
arc v113 v013 (calc(magn*aLidY) calc(magn*aLidX) air)

// Cylinder mantel (r-distance):
arc v200 v220 (calc(c30*r) calc(s30*r) depth)
arc v220 v020 (calc(s30*r) calc(c30*r) depth)

arc v201 v221 (calc(c30*r) calc(s30*r) top)
arc v221 v021 (calc(s30*r) calc(c30*r) top)

arc v202 v222 (calc(c30*r) calc(s30*r) top2)
arc v222 v022 (calc(s30*r) calc(c30*r) top2)

arc v203 v223 (calc(c30*r*magn) calc(s30*r*magn) air)
arc v223 v023 (calc(s30*r*magn) calc(c30*r*magn) air)

// r2 arcs:
arc v300 v330 (calc(c30*r2) calc(s30*r2) depth)
arc v330 v030 (calc(s30*r2) calc(c30*r2) depth)

arc v301 v331 (calc(c30*r2) calc(s30*r2) top)
arc v331 v031 (calc(s30*r2) calc(c30*r2) top)

arc v302 v332 (calc(c30*r2) calc(s30*r2) top2)
arc v332 v032 (calc(s30*r2) calc(c30*r2) top2)

arc v303 v333 (calc(magn*c30*r2) calc(magn*s30*r2) air)
arc v333 v033 (calc(magn*s30*r2) calc(magn*c30*r2) air)

// r3 arcs:
arc v400 v440 (calc(c30*r3) calc(s30*r3) depth)
arc v440 v040 (calc(s30*r3) calc(c30*r3) depth)

arc v401 v441 (calc(c30*r3) calc(s30*r3) r3Top)
arc v441 v041 (calc(s30*r3) calc(c30*r3) r3Top)

arc v402 v442 (calc(c30*r3) calc(s30*r3) calc(magn*top2))
arc v442 v042 (calc(s30*r3) calc(c30*r3) calc(magn*top2))

arc v403 v443 (calc(c30*r3) calc(s30*r3) air)
arc v443 v043 (calc(s30*r3) calc(c30*r3) air)

);

boundary        
(
	cylinder
	{
		type wall;
		faces
		(
			(v200 v220 v221 v201)
			(v020 v220 v221 v021)
			(v001 v101 v111 v011)
			(v101 v201 v221 v111)
			(v011 v111 v221 v021)			
		);
	}
	
	ambient
	{
		type patch;
		faces
		(
			(v400 v440 v441 v401)
			(v401 v441 v442 v402)
			(v402 v442 v443 v403)
			(v040 v440 v441 v041)
			(v041 v441 v442 v042)
			(v042 v442 v443 v043)

			// lid
			(v003 v103 v113 v013)
			(v103 v203 v223 v113)
			(v013 v113 v223 v023)			
			// top rings:
			(v203 v303 v333 v223)
			(v023 v223 v333 v033)
			(v303 v403 v443 v333)
			(v033 v333 v443 v043)
			
		);
	}
	
);

mergePatchPairs
(
);

