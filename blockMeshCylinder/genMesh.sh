#!/bin/sh
cd ${0%/*} || exit 1    # run from this directory

# $1 is diameter, $2 is resolution 

## First: make mesh as is
mkdir meshLogs

python meshquant.py $1 $2

m4 cylinder.m4 >system/blockMeshDict; 
blockMesh > meshLogs/blockMesh.out

# Mirror mesh in X
sed -i 's/normalVector .*/normalVector (-1 0 0);/g' system/mirrorMeshDict
mirrorMesh -overwrite >meshLogs/mirrorX.out;
# Mirror in Y
sed -i 's/normalVector .*/normalVector (0 -1 0);/g' system/mirrorMeshDict
mirrorMesh -overwrite >meshLogs/mirrorY.out;

# Mirror in Z
sed -i 's/normalVector .*/normalVector (0 0 -1);/g' system/mirrorMeshDict
mirrorMesh -overwrite >meshLogs/mirrorZ.out;

# Checkmesh
checkMesh >& meshLogs/checkMesh1.out

# Snappy hex to add layers
snappyHexMesh -overwrite -dict system/addLayerDict >meshLogs/snappyLayer.out

# Checkmesh again
checkMesh >& meshLogs/checkMesh2.out


# Store mesh: 
mkdir meshes
mv meshLogs constant/polyMesh/logs
cp -r constant/polyMesh meshes/D${1}_${2}x

