Cylinder mesh with boundary layer
=================================

This case sets up a cylinder with a 10 cell large boundary layer. 

Dependencies
------------ 
You'll need - 
	- python, for setting the domain discretisation parameters
 	- m4, for building blockMeshDict
	- blockMesh, for building the mesh 
	- snappyHexMesh, for layer addition

Usage
-----

./genMesh <cylinderDiameter> <refinementFactor> 

Built for cylinders of constant volume (8.4m3). 
Diameters tested (m): 2.1, 1.8, 1.4, 1.2
Refinements: 1.0 , 2.0, 3.0, 4.0, 8.0 
example: ./genMesh 2.1 2.0


Output
------
The resulting mesh is of course in the constant/polyMesh dict. 
It is also saved in a  `meshes/D<diameter>_<refinementFactor>x` together with all mesh logs ( in subfolder logs).


Johannes Palm
2019-04-29

