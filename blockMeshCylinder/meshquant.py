import numpy as np
import os, glob
## Print a m4 define statement to file f. frmt needed to make python print it correctly. (neewb thing)
def m4define(f,name,val,frmt):
	# Print m4 definition syntax
	f.write('// ----- ' + name + '-----//\n')
	frmtStr = '%s%'+frmt+'%s\n'
	f.write( frmtStr % ('define('+name+',', val , ')') )

## Main function
def main(args):

    ## Collect command line input ##
    # Cylinder radius:
    r=float(args[0])/2.0    
    # Refinement factor: 
    refFact = float(args[1]); 
    
    ## Resolution control parameters ##
    Vconst = 8.4; # m3 constant volume
    
    # Radial points, domain size
    r2Fact = 2
    r3Fact = 10
    
    # Vertical positions, domain size
    top2Fact = 2 # times cylinder height
    airFact= 5 # times cylinder height    

    # Angular resolution
    Ntheta = 3*refFact
    
    # Radial resolution 
    Nr1 = 2*refFact
    Nr = 3*refFact
    Nr2 = 6*refFact
    
    # Vertical resolution
    z0Grad = 1
    z1Grad = 2    

    # Dependent variables #
    h= 0.5*Vconst/(np.pi*r*r) # 1.2848
    hod = h/r   
    top2 = top2Fact*h   
    air = airFact*h
    r2 = r2Fact*r
    r3 = r3Fact*r
    
    # Mantel mesh size
    delta = 0.25*np.pi*r/Ntheta

    # Dependent resolutions
    Nz0 = round(h/delta)
    Nz1 = round(Nz0/1.5) 
    Nz2 = round(Nz1/1.5)	
	
	
    # Open output file
    of = open('domain.m4','w')
    
    # Print mesh settings
    m4define(of,'r',r,'f')
    m4define(of,'top',h,'f')
    m4define(of,'Ntheta',Ntheta,'d')
    m4define(of,'Nz0',Nz0,'d')
    m4define(of,'Nz1',Nz1,'d')
    m4define(of,'Nz2',Nz2,'d')    
    m4define(of,'z0Grad',z0Grad,'f')
    m4define(of,'z1Grad',z1Grad,'f')        
    
    m4define(of,'Nr1',Nr1,'d')
    m4define(of,'Nr',Nr,'d')
    m4define(of,'Nr2',Nr2,'d')
    
    
    # Print also new finalLayerThickness in addLayers file: 
    bashCmnd = "sed -i 's/finalLayerThickness .*/finalLayerThickness "+ str(delta/1.5) + ";/g' system/addLayerDict"
    print(bashCmnd)
    os.system(bashCmnd)
          

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
