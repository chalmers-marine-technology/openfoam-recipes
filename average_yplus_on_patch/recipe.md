# Compute the average $`y^+`$ on a patch at each time-step

A use-case for this is conducting an LES of turbulent channel flow, in which case you may want to monitor how the average $`y^+`$ on the walls develops in time.
When the value stabalises, the transients associated with initial conditions are gone, meaning that it is a good point to start time-averaging data.
More generally, this recipe shows how to compute aggregated data (sum, average, etc) of expressions defined on patches.

## Prerequisites

* `swak4foam` installed, all versions from the last 6 years will work.
* Any version of OpenFOAM, at least starting 2.1 from the Foundation.

The above is a big benefit of using `swak4foam`, even if the same thing can be done with OpenFOAM natively: `swak` compiles for all major OpenFOAM versions, and it is very careful about breaking changes.
All the things you do now are likely to work in a five years time.

## The recipe

In the `controlDict`, include the `swak4foam` libraries.

```
libs
(
   "libsimpleFunctionObjects.so"
   "libsimpleSwakFunctionObjects.so"
   "libswakFunctionObjects.so"
);
```
Now add the following to the `functions{}` inside the `controlDict`.

```
yPlus
{
    type patchExpression;
    patches
    (
        patchName
    );
    outputControlMode timeStep;
    outputInterval 1;
    expression "dist()/nu*sqrt((nu+nut)*mag(snGrad(U)))";
    verbose yes;
    writeStartTime no;
    accumulations
    (
        average // Replace with "banana" to see what other possibilities there are
    );
}
```

The `expression` reads as $`y/\nu \sqrt{(\nu + \nu_t)|dU/dy|}`$, where the expression under the square root is the wall-shear stress $`\tau_w`$, and $`y`$ is the wall-normal distance to the centre of the wall-adjacent cell. The SGS viscosity $`\nu_t`$ is included in case you are running a wall-modelled LES, otherwise $`\nu_t=0`$ at the wall and won't affect the results.

