# Compile Google Test and Google Mock using OpenFOAM compiler flags

Can be of interest for those that want to write unit tests using Google Test.
This guide is adapted from https://sourceflux.de/blog/google-test-and-openfoam.
Note that you need `cmake` and `python` installed prior to continuing.
The recipe currently only works with Foundation releases.

- Got to the directory where you want to store Google Test and run `git clone https://github.com/google/googletest`

- Copy the `get_wmake_compiler_options.py` script found in this directory to the `googletest` folder.
This script is a slightly modified version of that from **sourceflux**, namely, it attempts to handle the possibility of comiler flags spread over several lines.

- In `googletest/googletest/CMakeLists.txt` add the following
````
execute_process(COMMAND getWmakeCompilerOptions OUTPUT_VARIABLE WMAKE_COMPILER_OPTIONS)

if (WMAKE_COMPILER_OPTIONS STREQUAL "")
    MESSAGE(FATAL_ERROR "Cannot find 'getWmakeCompilerOptions'.")
endif()

message(STATUS "Extending the compiler options with following wmake options: ${WMAKE_COMPILER_OPTIONS}")
````
Just above the line `cxx_library(gtest "${cxx_strict}" src/gtest-all.cc)`. Then modify the `cxx_library` lines for the Google Test libraries so that `${cxx_strict}` is replaced by the string `{$WMAKE_COMPILER_OPTIONS}`.

- Do the same thing in `googletest/googlemock/CMakeLists.txt` above the line `cxx_library(gmock "${WMAKE_COMPILER_OPTIONS}" src/gmock-all.cc)$`.

- Create a build directory inside `googletest`, with `mkdir $WM_ARCH$WM_COMPILER$WM_COMPILE_OPTION`.

- `cd` to the created directory and run `cmake -DBUILD_SHARED_LIBS=ON ..`.

- Run `make`.

- Copy the generated libraries: `cp lib/* $FOAM_USER_LIBBIN`



