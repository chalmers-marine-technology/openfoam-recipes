import os 
from os import path 
import re

def main(): 

    # Start building the compiler call string with the WM_CXXFLAGS variable.

    WM_CXXFLAGS = "" 

    try: 
        WM_CXXFLAGS = os.environ["WM_CXXFLAGS"]
    except:
        print("OpenFOAM/foam-extend not configured. Exiting.")
        sys.exit(1)

    compilerOptions = os.environ["WM_CXXFLAGS"] + " "

    # Get the main C++ compiler option file from wmake.
    cppFile =  path.join(os.environ["WM_DIR"], "rules", \
                         os.environ["WM_ARCH"] + \
                         os.environ["WM_COMPILER"], "c++")

    # Expand the compiler options with c++Warn and ptFlags from cppFile.
    flag = False
    for line in open(cppFile,'r'):
        lineSplit = [x.rstrip(" ").lstrip(" ").rstrip("\n") for x in line.split("=")] 

        if flag:
            compilerOptions += lineSplit[0] + " "
            flag = False

        if (lineSplit[0] == "c++WARN") or (lineSplit[0] == "ptFLAGS"): 
            compilerOptions += lineSplit[1] + " " 
            ind = compilerOptions.find('\\')
            if ind > -1:
                compilerOptions = compilerOptions.replace('\\', ' ')
                # We need to read the next line due to \
                flag = True

    # Get the compilation option file (Opt, Prof, Debug)
    cppOptFile =  cppFile + os.environ["WM_COMPILE_OPTION"]

    # Get the c++OPT and c++DBUG
    for line in open(cppOptFile, 'r'): 
        lineSplit = [x.rstrip(" ").lstrip(" ").rstrip("\n") for x in line.split("=")] 
        if (lineSplit[0] == "c++OPT") or (lineSplit[0] == "c++DBUG"): 
            compilerOptions += lineSplit[1] + " " 

    print(compilerOptions)

if __name__ == "__main__":
    main()
